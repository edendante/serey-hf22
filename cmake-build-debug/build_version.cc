#include "build_version.h"
const char* rocksdb_build_git_sha = "rocksdb_build_git_sha:@0@";
const char* rocksdb_build_git_date = "rocksdb_build_git_date:@2019/03/14 03:30:31@";
const char* rocksdb_build_compile_date = __DATE__;
